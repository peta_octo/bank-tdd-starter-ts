import {it} from "@jest/globals";
import {BankController} from "./BankController";
import {Sortie} from "./Sortie";
import {instance, mock, verify} from "ts-mockito";

it('affiche un message de bienvenue', () => {
    const sortieMock = mock(Sortie)
    const controller = new BankController(instance(sortieMock))
    const messageAttendu = 'Bienvenue chez vous! Votre argent est en sécurité chez TDD-Bank, votre banque conçue en TDD.';

    controller.afficherMessageDeBienvenue()

    verify(sortieMock.afficher(messageAttendu)).called()
});
