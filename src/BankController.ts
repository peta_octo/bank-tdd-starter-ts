import {Sortie} from "./Sortie";

export class BankController {
    private sortie: Sortie;
    constructor(console: Sortie) {
        this.sortie = console;
    }

    afficherMessageDeBienvenue() {
        const messageDeBienvenue = 'Bienvenue chez vous! ' +
            'Votre argent est en sécurité chez TDD-Bank, ' +
            'votre banque conçue en TDD.';

        this.sortie.afficher(messageDeBienvenue)
    }

    interpréter(entrée: string) {
        throw new Error("À vous de jouer!")
    }
}

