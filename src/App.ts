import * as readline from 'readline';
import {BankController} from "./BankController";
import {Sortie} from "./Sortie";

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const bankController = new BankController(new Sortie());
bankController.afficherMessageDeBienvenue();
demanderUneEntrée();

function demanderUneEntrée() {
    rl.question('Saisir une commande à effectuer sur votre compte (ou \'quitter\' pour partir): ', (entrée) => {
        if (entrée === 'quitter') {
            rl.close();
            return
        }

        bankController.interpréter(entrée)

        console.log(`La demande ${entrée} n'est pas prise en charge par la banque.`)

        demanderUneEntrée();
    });
}
