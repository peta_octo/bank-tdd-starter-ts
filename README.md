# Banque

## Premier scénario

[saisie] depot 100

[saisie] retrait 10

[saisie] solde

~~~
Votre solde est de 90 euros.
~~~

## Deuxième scénario

[saisie] depot 100

[saisie] retrait 10

[saisie] historique

~~~
Date | Transaction | Solde après transaction
27/03/2024 | 100€ | 100€
28/03/2024 | -10€ | 90€
~~~

## Nouvelle demande métier

[saisie] depot 100

[saisie] retrait 10

[saisie] historique

~~~
Date | Transaction | Solde après transaction
27/03/2024 14:23 | 100€ | 100€
27/03/2024 14:24 | -10€ | 90€
~~~
